1
00:00:00,399 --> 00:00:03,679
reading code to try to find all the

2
00:00:02,080 --> 00:00:05,680
vulnerabilities in it is a

3
00:00:03,679 --> 00:00:07,600
time-consuming endeavor that requires a

4
00:00:05,680 --> 00:00:09,280
certain level of expertise and

5
00:00:07,600 --> 00:00:11,280
consequently could be error-prone you

6
00:00:09,280 --> 00:00:13,120
know given how experienced particular

7
00:00:11,280 --> 00:00:14,880
vulnerability hunter is

8
00:00:13,120 --> 00:00:16,880
now wouldn't it be nice if there were

9
00:00:14,880 --> 00:00:19,199
just tools that we could use in order to

10
00:00:16,880 --> 00:00:21,680
just find all the vulnerabilities for us

11
00:00:19,199 --> 00:00:23,840
well there are but you know this has to

12
00:00:21,680 --> 00:00:26,400
be taken with a grain of salt because if

13
00:00:23,840 --> 00:00:28,320
you think back to your computer science

14
00:00:26,400 --> 00:00:30,720
there's the halting problem that tells

15
00:00:28,320 --> 00:00:33,280
us that program analysis to try to

16
00:00:30,720 --> 00:00:36,399
determine such a simple criteria of will

17
00:00:33,280 --> 00:00:38,960
the program halt or not is not actually

18
00:00:36,399 --> 00:00:41,200
feasible so if that's not feasible then

19
00:00:38,960 --> 00:00:43,440
finding much more complicated situation

20
00:00:41,200 --> 00:00:45,840
like vulnerabilities given completely

21
00:00:43,440 --> 00:00:48,160
unknown input is probably also an

22
00:00:45,840 --> 00:00:49,840
extremely difficult or computationally

23
00:00:48,160 --> 00:00:52,160
infeasible thing

24
00:00:49,840 --> 00:00:55,280
so indeed you know static analysis tools

25
00:00:52,160 --> 00:00:57,440
do exist and they can be helpful but you

26
00:00:55,280 --> 00:00:59,280
have to recognize their limitations

27
00:00:57,440 --> 00:01:01,520
now these are called static analysis

28
00:00:59,280 --> 00:01:04,400
tools to differentiate them from dynamic

29
00:01:01,520 --> 00:01:05,920
analysis tools so dynamic analysis is

30
00:01:04,400 --> 00:01:07,520
when you're actually running the code

31
00:01:05,920 --> 00:01:09,119
and analyzing what's happening and

32
00:01:07,520 --> 00:01:10,880
static analysis is when you don't

33
00:01:09,119 --> 00:01:12,720
actually run the code and you try to

34
00:01:10,880 --> 00:01:14,560
figure out how it will behave

35
00:01:12,720 --> 00:01:16,560
so the relevant marketing terms are

36
00:01:14,560 --> 00:01:19,040
static application security testing and

37
00:01:16,560 --> 00:01:20,799
source code analysis apparently had to

38
00:01:19,040 --> 00:01:22,799
go look that up I just call them static

39
00:01:20,799 --> 00:01:25,200
analysis tools

40
00:01:22,799 --> 00:01:27,680
now if you work for a large tech company

41
00:01:25,200 --> 00:01:29,040
or someone that already has access to a

42
00:01:27,680 --> 00:01:30,880
static analysis tool you should

43
00:01:29,040 --> 00:01:32,799
absolutely use it

44
00:01:30,880 --> 00:01:35,200
you just like I said have to recognize

45
00:01:32,799 --> 00:01:37,360
that there are limitations most notably

46
00:01:35,200 --> 00:01:39,520
they do tend to have a lot of false

47
00:01:37,360 --> 00:01:41,119
positives and false negatives false

48
00:01:39,520 --> 00:01:43,360
positive is when they tell you something

49
00:01:41,119 --> 00:01:45,439
is vulnerability but it isn't actually

50
00:01:43,360 --> 00:01:47,360
and false negative is when they do not

51
00:01:45,439 --> 00:01:48,479
detect a vulnerability that is actually

52
00:01:47,360 --> 00:01:50,720
there

53
00:01:48,479 --> 00:01:52,560
so you know the fact of the matter is if

54
00:01:50,720 --> 00:01:54,960
you were just going to sit around and

55
00:01:52,560 --> 00:01:57,520
read the code with your ACID goggles

56
00:01:54,960 --> 00:02:00,560
anyways then of course it benefits you

57
00:01:57,520 --> 00:02:01,520
to start from the particular hits that a

58
00:02:00,560 --> 00:02:03,520
given

59
00:02:01,520 --> 00:02:05,600
static analysis tool may give you right

60
00:02:03,520 --> 00:02:07,680
it'll give you ins with the source code

61
00:02:05,600 --> 00:02:09,920
for places where there are potential

62
00:02:07,680 --> 00:02:11,760
errors typically static analysis tools

63
00:02:09,920 --> 00:02:14,160
are going to find the things like

64
00:02:11,760 --> 00:02:16,080
docker-controlled input coming in from

65
00:02:14,160 --> 00:02:18,480
environment variables or over the

66
00:02:16,080 --> 00:02:20,640
network or over the file system and then

67
00:02:18,480 --> 00:02:22,560
they will be looking for these ACID

68
00:02:20,640 --> 00:02:25,280
sinks which are you know the things like

69
00:02:22,560 --> 00:02:26,800
mem copies string copies etc so again

70
00:02:25,280 --> 00:02:28,800
you should absolutely use it for what

71
00:02:26,800 --> 00:02:32,160
it's worth but it's not always worth

72
00:02:28,800 --> 00:02:34,160
much so uh the government nist sorry the

73
00:02:32,160 --> 00:02:35,599
us government nist in particular

74
00:02:34,160 --> 00:02:36,480
national institute for standards and

75
00:02:35,599 --> 00:02:39,280
technology

76
00:02:36,480 --> 00:02:42,560
has run analysis of a variety of static

77
00:02:39,280 --> 00:02:44,400
analysis tools uh many years and the

78
00:02:42,560 --> 00:02:46,959
latest report that I was able to find

79
00:02:44,400 --> 00:02:49,120
was the 2018 one

80
00:02:46,959 --> 00:02:51,440
and basically what you see when you look

81
00:02:49,120 --> 00:02:53,920
at these reports is that you know they

82
00:02:51,440 --> 00:02:56,080
look at certain simple medium or

83
00:02:53,920 --> 00:02:57,680
extremely complicated vulnerabilities

84
00:02:56,080 --> 00:02:59,680
and then they evaluate a bunch of tools

85
00:02:57,680 --> 00:03:01,680
to say did they catch it or did they

86
00:02:59,680 --> 00:03:04,480
miss it now the nist report is

87
00:03:01,680 --> 00:03:06,159
purposefully anonymizing which tools it

88
00:03:04,480 --> 00:03:08,239
is actually looking at

89
00:03:06,159 --> 00:03:10,159
so basically they don't want to be seen

90
00:03:08,239 --> 00:03:12,879
as recommending or disrecommending any

91
00:03:10,159 --> 00:03:14,879
given tool but you know universally the

92
00:03:12,879 --> 00:03:17,599
conclusions of both you know nist

93
00:03:14,879 --> 00:03:19,840
reports and nsa reports is that there's

94
00:03:17,599 --> 00:03:22,000
a whole lot of you know false positives

95
00:03:19,840 --> 00:03:23,840
and generally speaking uh between

96
00:03:22,000 --> 00:03:26,319
different tools there are will

97
00:03:23,840 --> 00:03:27,920
oftentimes be sort of different finds so

98
00:03:26,319 --> 00:03:29,519
essentially they recommend you know

99
00:03:27,920 --> 00:03:32,159
using multiple tools which are going to

100
00:03:29,519 --> 00:03:35,519
find multiple different vulnerabilities

101
00:03:32,159 --> 00:03:37,760
but but yeah again the point is just to

102
00:03:35,519 --> 00:03:39,840
make it clear to you that these are not

103
00:03:37,760 --> 00:03:41,680
a just put to code in and all the

104
00:03:39,840 --> 00:03:44,000
vulnerabilities will pop out these are

105
00:03:41,680 --> 00:03:47,040
just like a way of accelerating the

106
00:03:44,000 --> 00:03:49,040
triage that must necessarily occur

107
00:03:47,040 --> 00:03:51,360
via human analysis

108
00:03:49,040 --> 00:03:53,360
now as I mentioned before the static

109
00:03:51,360 --> 00:03:56,720
analysis tools will generally have some

110
00:03:53,360 --> 00:04:00,480
pre-built in sources of tainted data the

111
00:03:56,720 --> 00:04:02,400
ACID sources and the ACID sinks the

112
00:04:00,480 --> 00:04:04,319
tainted data sources tainted data syncs

113
00:04:02,400 --> 00:04:06,239
as a reminder tainted data is the actual

114
00:04:04,319 --> 00:04:08,239
proper industry term and ACID is just a

115
00:04:06,239 --> 00:04:10,319
made-up term I use for this class

116
00:04:08,239 --> 00:04:11,760
so anyways they have some default things

117
00:04:10,319 --> 00:04:14,400
for that

118
00:04:11,760 --> 00:04:16,720
but you know to my mind customization

119
00:04:14,400 --> 00:04:18,720
of these things is key to actually

120
00:04:16,720 --> 00:04:20,720
getting good results out of them in

121
00:04:18,720 --> 00:04:22,960
particular you know in my context when i

122
00:04:20,720 --> 00:04:25,120
work in firmware security you know sure

123
00:04:22,960 --> 00:04:27,280
they know about things like data read in

124
00:04:25,120 --> 00:04:29,199
from a socket if it's literally socket

125
00:04:27,280 --> 00:04:31,520
in that posix sense but they don't know

126
00:04:29,199 --> 00:04:33,520
anything about the fact that data that i

127
00:04:31,520 --> 00:04:36,160
consider data tainted if it's being read

128
00:04:33,520 --> 00:04:37,040
in from nvram in UEFI or something like

129
00:04:36,160 --> 00:04:38,960
that

130
00:04:37,040 --> 00:04:41,040
so you know it is required to actually

131
00:04:38,960 --> 00:04:44,080
customize these to get the best possible

132
00:04:41,040 --> 00:04:45,919
options but even customization can be

133
00:04:44,080 --> 00:04:47,840
particularly tricky and you know you

134
00:04:45,919 --> 00:04:50,080
work with the customer support to try to

135
00:04:47,840 --> 00:04:51,759
get things changed and sometimes it

136
00:04:50,080 --> 00:04:53,520
works and sometimes it doesn't and most

137
00:04:51,759 --> 00:04:56,960
of the time it doesn't in my experience

138
00:04:53,520 --> 00:04:58,720
so anyways the key thing is that again

139
00:04:56,960 --> 00:05:00,320
they're great for like triage and

140
00:04:58,720 --> 00:05:01,840
catching some easy bugs right don't get

141
00:05:00,320 --> 00:05:04,639
me wrong there's absolutely nothing

142
00:05:01,840 --> 00:05:06,080
wrong with getting those easy wins by

143
00:05:04,639 --> 00:05:08,880
you know taking and looking at their

144
00:05:06,080 --> 00:05:10,639
output sorting out the false positives

145
00:05:08,880 --> 00:05:12,560
and then you know addressing any of the

146
00:05:10,639 --> 00:05:14,240
things they find but you know you have

147
00:05:12,560 --> 00:05:16,240
to recognize that there's false

148
00:05:14,240 --> 00:05:18,240
positives false negatives a lot of times

149
00:05:16,240 --> 00:05:20,240
false negatives around inter-procedural

150
00:05:18,240 --> 00:05:22,560
analysis meaning they can't properly

151
00:05:20,240 --> 00:05:24,479
track the ACID going into functions and

152
00:05:22,560 --> 00:05:25,600
coming out of functions

153
00:05:24,479 --> 00:05:27,120
so

154
00:05:25,600 --> 00:05:29,360
you know it's the kind of thing where i

155
00:05:27,120 --> 00:05:31,280
do not recommend necessarily that a

156
00:05:29,360 --> 00:05:33,440
development team go off and try to work

157
00:05:31,280 --> 00:05:35,120
with the vendor to to customize it and

158
00:05:33,440 --> 00:05:36,639
stuff like that that's probably usually

159
00:05:35,120 --> 00:05:39,520
something more where you would want the

160
00:05:36,639 --> 00:05:41,199
security team if you have one to uh you

161
00:05:39,520 --> 00:05:42,880
know first of all do the first order

162
00:05:41,199 --> 00:05:45,440
triage of saying you know let's get rid

163
00:05:42,880 --> 00:05:47,280
of any obvious false positives and then

164
00:05:45,440 --> 00:05:49,919
you know they can hand you the

165
00:05:47,280 --> 00:05:53,039
uh the the possible false

166
00:05:49,919 --> 00:05:54,560
sorry positive possible true positives

167
00:05:53,039 --> 00:05:57,520
which then you know a developer could

168
00:05:54,560 --> 00:05:57,520
evaluate

