1
00:00:00,160 --> 00:00:04,560
stack canaries are an exploit mitigation

2
00:00:02,240 --> 00:00:07,279
technique that is based on the idea of

3
00:00:04,560 --> 00:00:09,440
the proverbial canary in a coal mine and

4
00:00:07,279 --> 00:00:11,599
this is based on the real action that

5
00:00:09,440 --> 00:00:14,240
miners would take to bring a canary a

6
00:00:11,599 --> 00:00:16,880
small bird with them into a coal mine

7
00:00:14,240 --> 00:00:19,199
and if there was the presence of toxic

8
00:00:16,880 --> 00:00:21,680
gases that would be harmful to humans

9
00:00:19,199 --> 00:00:24,000
the canary would be more susceptible to

10
00:00:21,680 --> 00:00:26,480
the gases and would consequently die in

11
00:00:24,000 --> 00:00:28,560
the presence of the toxins before the

12
00:00:26,480 --> 00:00:30,960
miners themselves would be hurt so the

13
00:00:28,560 --> 00:00:32,640
idea here is that the stack canary

14
00:00:30,960 --> 00:00:35,520
detects the presence of a toxic

15
00:00:32,640 --> 00:00:37,760
substance like ACID before it can cause

16
00:00:35,520 --> 00:00:39,920
any real damage these are also known in

17
00:00:37,760 --> 00:00:41,760
some implementations as stack cookies

18
00:00:39,920 --> 00:00:43,840
with the idea of you know breaking the

19
00:00:41,760 --> 00:00:45,120
cookie and cookies are not as good if

20
00:00:43,840 --> 00:00:47,039
they're broken

21
00:00:45,120 --> 00:00:48,960
but the idea is that you take a random

22
00:00:47,039 --> 00:00:50,480
number and you place it between the

23
00:00:48,960 --> 00:00:52,559
local variables which will be on the

24
00:00:50,480 --> 00:00:54,719
stack and the return address which will

25
00:00:52,559 --> 00:00:57,120
be on the stack and the idea is before

26
00:00:54,719 --> 00:00:59,199
you return from a function you check

27
00:00:57,120 --> 00:01:02,879
that random number and you say is it

28
00:00:59,199 --> 00:01:04,479
still uncorrupted and if so great return

29
00:01:02,879 --> 00:01:06,000
but if it is corrupted then you should

30
00:01:04,479 --> 00:01:07,840
not use the return address because it

31
00:01:06,000 --> 00:01:09,840
could have been subject to a stack

32
00:01:07,840 --> 00:01:11,280
buffer overflow

33
00:01:09,840 --> 00:01:13,600
so this is generally added by the

34
00:01:11,280 --> 00:01:15,680
compiler although you know you can hack

35
00:01:13,600 --> 00:01:17,360
them into binaries via dynamic

36
00:01:15,680 --> 00:01:18,799
instrumentation and stuff like that so

37
00:01:17,360 --> 00:01:21,119
there's a bunch of assumptions that come

38
00:01:18,799 --> 00:01:22,880
along with stack canaries the first is

39
00:01:21,119 --> 00:01:24,960
that it assumes that what you're trying

40
00:01:22,880 --> 00:01:26,799
to combat is a linear stack buffer

41
00:01:24,960 --> 00:01:28,880
overflow where it's going from the local

42
00:01:26,799 --> 00:01:31,439
variables and smashing the stack all the

43
00:01:28,880 --> 00:01:33,759
way up to the return address

44
00:01:31,439 --> 00:01:35,600
so if any of these assumptions are

45
00:01:33,759 --> 00:01:36,880
violated then the defense will not

46
00:01:35,600 --> 00:01:38,960
actually work that's again why it's

47
00:01:36,880 --> 00:01:41,200
called a mitigation it's not actually a

48
00:01:38,960 --> 00:01:42,320
fundamental prevention mechanism another

49
00:01:41,200 --> 00:01:44,479
assumption is that it's hard for

50
00:01:42,320 --> 00:01:46,240
attacker to guess the 32 or 64-bit

51
00:01:44,479 --> 00:01:47,840
random number and another is that it's

52
00:01:46,240 --> 00:01:49,360
possible it's not possible for the

53
00:01:47,840 --> 00:01:50,799
attacker to read the value and write

54
00:01:49,360 --> 00:01:52,960
back the correct one

55
00:01:50,799 --> 00:01:54,799
so if we go back and we consider the

56
00:01:52,960 --> 00:01:57,280
basic stack overflow and we've got you

57
00:01:54,799 --> 00:01:59,680
know memcpy from an ACID buff of ACID

58
00:01:57,280 --> 00:02:01,920
length into the vulnerable buffer

59
00:01:59,680 --> 00:02:04,560
then the idea here would be that it goes

60
00:02:01,920 --> 00:02:06,960
and it copies and then it smashes the

61
00:02:04,560 --> 00:02:09,039
stack canary before it ultimately

62
00:02:06,960 --> 00:02:11,520
smashes the return address

63
00:02:09,039 --> 00:02:13,520
and so then there is going to be a check

64
00:02:11,520 --> 00:02:15,680
before you return that says is this

65
00:02:13,520 --> 00:02:17,440
location right here still the random

66
00:02:15,680 --> 00:02:20,560
number is it still the correct and

67
00:02:17,440 --> 00:02:23,360
uncorrupted secondary if so then great

68
00:02:20,560 --> 00:02:26,000
return if not don't return because the

69
00:02:23,360 --> 00:02:27,599
return address could be asset

70
00:02:26,000 --> 00:02:29,440
now like I said there's assumptions and

71
00:02:27,599 --> 00:02:30,879
so each of these could be violated and

72
00:02:29,440 --> 00:02:32,879
turned by an attacker depending on the

73
00:02:30,879 --> 00:02:34,720
situation so for instance the assumption

74
00:02:32,879 --> 00:02:36,959
that it's a linear writing buffer

75
00:02:34,720 --> 00:02:38,879
overflow could be violated if the

76
00:02:36,959 --> 00:02:41,360
attacker has control over the

77
00:02:38,879 --> 00:02:44,400
destination pointer in that case they

78
00:02:41,360 --> 00:02:46,480
could perhaps you know skip up here and

79
00:02:44,400 --> 00:02:48,160
cause the buffer overflow to just

80
00:02:46,480 --> 00:02:50,080
overwrite the return address without

81
00:02:48,160 --> 00:02:51,440
actually corrupting the stat canary and

82
00:02:50,080 --> 00:02:53,120
then again you know these are the other

83
00:02:51,440 --> 00:02:54,480
assumptions that you know for instance

84
00:02:53,120 --> 00:02:56,480
that it's hard for an attacker to guess

85
00:02:54,480 --> 00:02:57,920
the 32-bit number well that all comes

86
00:02:56,480 --> 00:02:59,120
down to the implementation and if

87
00:02:57,920 --> 00:03:00,879
perhaps the

88
00:02:59,120 --> 00:03:02,720
implementers did not have adequate

89
00:03:00,879 --> 00:03:04,560
randomization maybe it's much much

90
00:03:02,720 --> 00:03:06,720
easier for the attacker to guess a

91
00:03:04,560 --> 00:03:08,400
possible number that will succeed and

92
00:03:06,720 --> 00:03:10,000
then also the assumption it's not

93
00:03:08,400 --> 00:03:11,760
possible for the attacker to read the

94
00:03:10,000 --> 00:03:13,440
canary because if they can read the

95
00:03:11,760 --> 00:03:15,680
canary then they can write back the

96
00:03:13,440 --> 00:03:17,920
exact correct value while they're doing

97
00:03:15,680 --> 00:03:20,400
their linear buffer overflow and so you

98
00:03:17,920 --> 00:03:22,400
know violation of that assumption is

99
00:03:20,400 --> 00:03:24,319
down to vulnerability class called

100
00:03:22,400 --> 00:03:25,760
information disclosure or info leaks

101
00:03:24,319 --> 00:03:27,280
that will be talked about in a future

102
00:03:25,760 --> 00:03:28,879
class

103
00:03:27,280 --> 00:03:30,959
at the end of the day stationaries are

104
00:03:28,879 --> 00:03:33,599
an extremely cheap exploit mitigation

105
00:03:30,959 --> 00:03:36,319
mechanism which is important and should

106
00:03:33,599 --> 00:03:38,080
be enabled in all locations you can see

107
00:03:36,319 --> 00:03:42,120
the website for more details about how

108
00:03:38,080 --> 00:03:42,120
to enable it in different compilers

