1
00:00:00,080 --> 00:00:04,160
this vulnerability doesn't have a CVE

2
00:00:02,159 --> 00:00:06,319
associated with it because it was a

3
00:00:04,160 --> 00:00:08,800
Microsoft internal find that they

4
00:00:06,319 --> 00:00:10,559
presented at the BlackHat 2020

5
00:00:08,800 --> 00:00:12,480
conference

6
00:00:10,559 --> 00:00:15,440
so this has to do with virtualization

7
00:00:12,480 --> 00:00:16,960
based security which is where Microsoft

8
00:00:15,440 --> 00:00:19,279
uses the hardware support for

9
00:00:16,960 --> 00:00:21,680
virtualization that exists on all modern

10
00:00:19,279 --> 00:00:24,400
systems in order to split the Windows

11
00:00:21,680 --> 00:00:28,160
kernel into a more secure version which

12
00:00:24,400 --> 00:00:29,439
runs more limited functionality and the

13
00:00:28,160 --> 00:00:31,279
normal one

14
00:00:29,439 --> 00:00:33,760
they call the secure version the one

15
00:00:31,279 --> 00:00:35,840
that runs at virtual trust level one and

16
00:00:33,760 --> 00:00:37,280
the normal one runs at virtual trust

17
00:00:35,840 --> 00:00:40,559
level zero

18
00:00:37,280 --> 00:00:42,640
so if we zoom in on that VTL0 that's the

19
00:00:40,559 --> 00:00:44,079
normal kernel to a normal to a user

20
00:00:42,640 --> 00:00:46,239
whose turn on virtualization based

21
00:00:44,079 --> 00:00:48,480
security it just looks like a normal

22
00:00:46,239 --> 00:00:50,079
window system but behind the scenes

23
00:00:48,480 --> 00:00:53,360
they've got

24
00:00:50,079 --> 00:00:56,000
the secure kernel running at VTL1 in a

25
00:00:53,360 --> 00:00:57,600
completely separate virtual machine

26
00:00:56,000 --> 00:00:59,840
so the particular vulnerability that

27
00:00:57,600 --> 00:01:02,480
we're going to see here has to do with

28
00:00:59,840 --> 00:01:04,640
ACID flowing from the normal kernel to

29
00:01:02,480 --> 00:01:06,720
the secure kernel

30
00:01:04,640 --> 00:01:09,280
and so of course that means that you

31
00:01:06,720 --> 00:01:11,280
know this constitutes an attack surface

32
00:01:09,280 --> 00:01:12,640
and the secure kernel because it's

33
00:01:11,280 --> 00:01:15,439
supposed to be secure should always

34
00:01:12,640 --> 00:01:17,119
distrust anything outside of itself so

35
00:01:15,439 --> 00:01:19,920
you know if you were to look at this the

36
00:01:17,119 --> 00:01:21,759
correct paranoid way you would say that

37
00:01:19,920 --> 00:01:23,680
you know the secure kernel shouldn't

38
00:01:21,759 --> 00:01:26,479
trust the normal kernel shouldn't trust

39
00:01:23,680 --> 00:01:28,799
the kernel integrity verification code

40
00:01:26,479 --> 00:01:30,880
that runs over here in this vm to verify

41
00:01:28,799 --> 00:01:33,520
this kernel kernel shouldn't trust

42
00:01:30,880 --> 00:01:35,119
Windows platform services apps shouldn't

43
00:01:33,520 --> 00:01:37,200
trust each other hypervisor shouldn't

44
00:01:35,119 --> 00:01:39,280
trust the hardware because remember you

45
00:01:37,200 --> 00:01:41,360
need to program paranoid because it's

46
00:01:39,280 --> 00:01:42,560
not paranoia if they really are out to

47
00:01:41,360 --> 00:01:44,479
get you

48
00:01:42,560 --> 00:01:47,600
so anyways architecturally the point of

49
00:01:44,479 --> 00:01:50,079
splitting the VTL1 kernel and the VTL0

50
00:01:47,600 --> 00:01:53,200
is to reduce the attack surface on the

51
00:01:50,079 --> 00:01:55,600
VTL1 so the expectation is that you know

52
00:01:53,200 --> 00:01:58,240
these days the normal Windows kernel is

53
00:01:55,600 --> 00:02:00,399
extremely large it's basically

54
00:01:58,240 --> 00:02:02,320
indefensible from the perspective of

55
00:02:00,399 --> 00:02:04,000
there's just so much legacy code you

56
00:02:02,320 --> 00:02:04,960
know millions and millions of lines of

57
00:02:04,000 --> 00:02:07,439
code

58
00:02:04,960 --> 00:02:09,759
and therefore if you have truly security

59
00:02:07,439 --> 00:02:12,640
critical functionality it will be broken

60
00:02:09,759 --> 00:02:14,800
out and placed into the VTL1 kernel so

61
00:02:12,640 --> 00:02:17,360
that that with a much more reduced

62
00:02:14,800 --> 00:02:19,040
attack surface can handle the security

63
00:02:17,360 --> 00:02:20,720
critical functionality and hopefully

64
00:02:19,040 --> 00:02:23,440
will be less

65
00:02:20,720 --> 00:02:25,599
easy to break into so again the VTL1

66
00:02:23,440 --> 00:02:28,480
kernel should be distrusting everything

67
00:02:25,599 --> 00:02:29,920
including inputs coming from the VTL0

68
00:02:28,480 --> 00:02:32,400
kernel

69
00:02:29,920 --> 00:02:34,000
now we're going to see MDLs a couple of

70
00:02:32,400 --> 00:02:35,760
times in this class for different

71
00:02:34,000 --> 00:02:38,239
different examples and for this

72
00:02:35,760 --> 00:02:41,040
particular exploitation the MDLs are

73
00:02:38,239 --> 00:02:42,640
going to be critical so MDLs are memory

74
00:02:41,040 --> 00:02:45,200
descriptor lists and they're what

75
00:02:42,640 --> 00:02:47,280
Microsoft calls a semi-opaque data

76
00:02:45,200 --> 00:02:49,519
structure meaning that officially this

77
00:02:47,280 --> 00:02:51,360
is not all documented only a couple of

78
00:02:49,519 --> 00:02:52,400
fields are documented and have official

79
00:02:51,360 --> 00:02:54,000
APIs

80
00:02:52,400 --> 00:02:55,599
but you know of course people have

81
00:02:54,000 --> 00:02:57,840
reverse engineered things and they know

82
00:02:55,599 --> 00:02:59,680
generally how it behaves so when you

83
00:02:57,840 --> 00:03:02,239
think about MDLs I want you to think

84
00:02:59,680 --> 00:03:04,319
about the notion of there's a fixed

85
00:03:02,239 --> 00:03:06,400
structure at the beginning of the MDL

86
00:03:04,319 --> 00:03:08,319
this MDL struct

87
00:03:06,400 --> 00:03:11,360
and then after the structure is a

88
00:03:08,319 --> 00:03:13,760
variable length array of pointers and

89
00:03:11,360 --> 00:03:15,519
these pointers are pointers to what are

90
00:03:13,760 --> 00:03:17,680
called frames

91
00:03:15,519 --> 00:03:21,360
or you know physical frames these are

92
00:03:17,680 --> 00:03:23,040
hex 1000 size chunks of memory which are

93
00:03:21,360 --> 00:03:25,200
you know defining the overall memory

94
00:03:23,040 --> 00:03:27,360
descriptor list and how it looks

95
00:03:25,200 --> 00:03:30,080
there's going to be a offset field in

96
00:03:27,360 --> 00:03:32,319
the MDL it says where the actual memory

97
00:03:30,080 --> 00:03:34,480
that's going to be declared it starts

98
00:03:32,319 --> 00:03:36,400
from within these frames and then

99
00:03:34,480 --> 00:03:38,480
there's going to be a byte count that

100
00:03:36,400 --> 00:03:40,159
indicates what the total size of this

101
00:03:38,480 --> 00:03:41,840
described memory is

102
00:03:40,159 --> 00:03:43,440
so overall that

103
00:03:41,840 --> 00:03:45,280
is going to be the described memory the

104
00:03:43,440 --> 00:03:47,920
MDL has got this structure as

105
00:03:45,280 --> 00:03:50,159
essentially a header it's got some array

106
00:03:47,920 --> 00:03:53,280
variable sized array of frames however

107
00:03:50,159 --> 00:03:56,319
many frames however many hex 1000 chunks

108
00:03:53,280 --> 00:03:58,239
of data are needed to describe the MDL

109
00:03:56,319 --> 00:03:59,599
and then this is going to describe the

110
00:03:58,239 --> 00:04:01,280
overall structure

111
00:03:59,599 --> 00:04:02,799
so this is you know how it would look in

112
00:04:01,280 --> 00:04:05,120
struct form

113
00:04:02,799 --> 00:04:07,280
this is how it looked in the research

114
00:04:05,120 --> 00:04:09,760
that we're going to be talking through

115
00:04:07,280 --> 00:04:11,360
and so here you've got that next pointer

116
00:04:09,760 --> 00:04:14,159
at the beginning that's you know 8 bytes

117
00:04:11,360 --> 00:04:16,479
assuming a 64-bit architecture two byte

118
00:04:14,159 --> 00:04:18,320
size two byte flags etc

119
00:04:16,479 --> 00:04:20,560
and then after this fixed size structure

120
00:04:18,320 --> 00:04:23,199
at the beginning is going to be the

121
00:04:20,560 --> 00:04:26,479
variable length array of what I call the

122
00:04:23,199 --> 00:04:29,600
page p frame and then a number and what

123
00:04:26,479 --> 00:04:33,840
Microsoft calls PFNs page frame numbers

124
00:04:29,600 --> 00:04:35,840
so PFN0 PFN1 etc there are addresses of

125
00:04:33,840 --> 00:04:37,520
physical frames of physical memory as

126
00:04:35,840 --> 00:04:40,400
opposed to virtual memory

127
00:04:37,520 --> 00:04:43,759
so this MDL has to do with its data

128
00:04:40,400 --> 00:04:46,479
that's going to be transferred from the

129
00:04:43,759 --> 00:04:48,160
insecure kernel from the VTL 0 kernel to

130
00:04:46,479 --> 00:04:50,720
the secure kernel

131
00:04:48,160 --> 00:04:52,800
and all of these content are ultimately

132
00:04:50,720 --> 00:04:54,800
going to be attack controlled values

133
00:04:52,800 --> 00:04:58,320
when they're passed from the

134
00:04:54,800 --> 00:05:00,400
VTL0 kernel to the VTL1 kernel and you

135
00:04:58,320 --> 00:05:01,840
know we're assuming here that the VTL0

136
00:05:00,400 --> 00:05:03,600
kernel has been completely

137
00:05:01,840 --> 00:05:05,199
compromised and the attacker is now

138
00:05:03,600 --> 00:05:08,000
trying to you know break into whatever

139
00:05:05,199 --> 00:05:10,320
security functionality exists over in

140
00:05:08,000 --> 00:05:12,479
the secure kernel world so the attacker

141
00:05:10,320 --> 00:05:16,800
controls all of this they're not going

142
00:05:12,479 --> 00:05:18,639
to control the mapped system va so the

143
00:05:16,800 --> 00:05:20,800
the secure world is going to be able to

144
00:05:18,639 --> 00:05:22,880
control that and set that but at the end

145
00:05:20,800 --> 00:05:25,120
of the day this map system va is just a

146
00:05:22,880 --> 00:05:27,360
virtual address that is going to point

147
00:05:25,120 --> 00:05:30,160
at the data contained with all of

148
00:05:27,360 --> 00:05:31,680
contained within all of these frames

149
00:05:30,160 --> 00:05:33,199
that are given by the array at the end

150
00:05:31,680 --> 00:05:35,680
so even though the attacker doesn't

151
00:05:33,199 --> 00:05:38,880
control the actual address if you see

152
00:05:35,680 --> 00:05:39,759
that used it is actually just a virtual

153
00:05:38,880 --> 00:05:42,080
address that points at

154
00:05:39,759 --> 00:05:44,080
attacker-controlled data

155
00:05:42,080 --> 00:05:46,320
all right so here is some pseudo code

156
00:05:44,080 --> 00:05:47,520
that was given in the presentation

157
00:05:46,320 --> 00:05:49,280
we've got

158
00:05:47,520 --> 00:05:51,120
some code that the first thing that it's

159
00:05:49,280 --> 00:05:52,960
doing this is on the secure kernel side

160
00:05:51,120 --> 00:05:54,960
of the world on the secure kernel side

161
00:05:52,960 --> 00:05:57,199
the first thing it does is it maps this

162
00:05:54,960 --> 00:06:00,240
data transfer that's coming from the

163
00:05:57,199 --> 00:06:01,840
VTL0 kernel into the VTL1

164
00:06:00,240 --> 00:06:04,560
and so I'm going to just tell you that

165
00:06:01,840 --> 00:06:06,160
this data MDL and this transfer PFN

166
00:06:04,560 --> 00:06:07,600
which we don't care as much about we're

167
00:06:06,160 --> 00:06:09,759
just going to say that this this is

168
00:06:07,600 --> 00:06:11,120
going to be the input and this is going

169
00:06:09,759 --> 00:06:13,600
to be the output so this is just

170
00:06:11,120 --> 00:06:15,840
essentially mapping it so that the

171
00:06:13,600 --> 00:06:17,840
physical addresses the information that

172
00:06:15,840 --> 00:06:19,840
the the insecure kernel told the secure

173
00:06:17,840 --> 00:06:22,080
kernel about will become available to

174
00:06:19,840 --> 00:06:23,120
the secure kernel via this transfer MDL

175
00:06:22,080 --> 00:06:25,360
so

176
00:06:23,120 --> 00:06:28,000
data in data out and then this is

177
00:06:25,360 --> 00:06:29,840
tainted and then I want you to go ahead

178
00:06:28,000 --> 00:06:31,280
and look at the source code to figure

179
00:06:29,840 --> 00:06:33,360
out what the implications of that

180
00:06:31,280 --> 00:06:36,080
tainted data are and where there is the

181
00:06:33,360 --> 00:06:37,919
potential for a heat buffer overflow

182
00:06:36,080 --> 00:06:39,840
also going to tell you just that this

183
00:06:37,919 --> 00:06:41,840
last little thing that looked like a

184
00:06:39,840 --> 00:06:44,000
function at the end of that code the mm

185
00:06:41,840 --> 00:06:46,160
initialize MDL is not actually a

186
00:06:44,000 --> 00:06:48,319
function it's a macro and it's just

187
00:06:46,160 --> 00:06:50,720
going to initialize fields within the

188
00:06:48,319 --> 00:06:52,720
MDL the memory descriptor list that is

189
00:06:50,720 --> 00:06:56,000
passed to it

190
00:06:52,720 --> 00:06:56,000
so with that go look at the code

